Basic beacon detection app using SwiftUI.

The app is using a hardcoded UUID of `5A4BCFCE-174E-4BAC-A814-092E77F6B7E5` with major of 123 and minor 456.

The UI simply contains a value if the beacon is near, far immediate or unknown along with the distance in meters. 