//
//  ContentView.swift
//  BeaconDetector
//
//  Created by Timothy on 14/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView : View {
    @ObjectBinding var beaconDetector = BeaconDetector()
    
    var body: some View {
        VStack {
            Text(beaconDetector.lastDistance.stringValue)
                .font(.system(size: 72, design: .rounded))
            Text(beaconDetector.lastAccuracy.stringValue)
                .font(.system(size: 40, design: .rounded))
        }
        .frame(minWidth: 0, idealWidth: 0, maxWidth: .infinity, minHeight: 0, idealHeight: 0, maxHeight: .infinity, alignment: .center)
        .background(beaconDetector.lastDistance.colorValue)
        .edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
