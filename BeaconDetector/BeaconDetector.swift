//
//  Beaco.swift
//  BeaconDetector
//
//  Created by Timothy on 14/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import Combine
import CoreLocation
import SwiftUI

class BeaconDetector: NSObject, BindableObject {
    
    var didChange = PassthroughSubject<Void, Never>()
    var locationManager: CLLocationManager?
    var lastDistance = CLProximity.unknown
    var lastAccuracy: CLLocationAccuracy = -1.0
    
    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.requestWhenInUseAuthorization()
    }
    
    func startScanning() {
        let uuid = UUID(uuidString: "5A4BCFCE-174E-4BAC-A814-092E77F6B7E5")!
        let constraint = CLBeaconIdentityConstraint(uuid: uuid, major: 123, minor: 456)
        let beaconRegion = CLBeaconRegion(beaconIdentityConstraint: constraint, identifier: "MyBeacon")
        self.locationManager?.startMonitoring(for: beaconRegion)
        self.locationManager?.startRangingBeacons(satisfying: constraint)
    }
    
    func update(_ distance: CLProximity, accuracy: CLLocationAccuracy) {
        self.lastDistance = distance
        self.lastAccuracy = accuracy
        self.didChange.send(())
    }
    
}

// MARK: CLLocationManagerDelegate
extension BeaconDetector: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self), CLLocationManager.isRangingAvailable() {
                self.startScanning()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
        if let beaon = beacons.first {
            self.update(beaon.proximity, accuracy: beaon.accuracy)
        } else {
            self.update(.unknown, accuracy: -1)
        }
    }
}


// MARK: CLProximity
extension CLProximity {
    
    var stringValue: String {
        switch self {
        case .far: return "FAR"
        case .immediate: return "IMMEDIATE"
        case .near: return "NEAR"
        case .unknown: return "UNKNOWN"
        @unknown default:
            return "ERROR"
        }
    }
    
    var colorValue: Color {
        switch self {
        case .far: return Color.orange
        case .immediate: return Color.green
        case .near: return Color.yellow
        case .unknown: return Color.gray
        @unknown default:
            return Color.red
        }
    }
}

// MARK: CLLocationAccuarcy
extension CLLocationAccuracy {
    var stringValue: String {
        if self > 0 {
            return "\(String(format: "%.2f", self)) meters"
        } else {
            return "Uknown distance"
        }
    }
}
